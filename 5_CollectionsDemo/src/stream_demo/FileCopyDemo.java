package stream_demo;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyDemo {

	public static void main(String[] args) throws IOException {
		try (BufferedInputStream bin =
				new BufferedInputStream(new FileInputStream("C:\\java\\file1.zip"));
			BufferedOutputStream bout =
				new BufferedOutputStream(new FileOutputStream("C:\\java\\file2.zip"))){
			int c;
			while ((c = bin.read()) != -1) {
				bout.write(c);
			}
		}
		System.out.println("Done");
	}

}
