package stream_demo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class StringToFile2 {

	public static void main(String[] args) throws IOException {
		String content = "Hello world \r\n java \r\n";
		String path  = "C:\\java\\file2.txt";
		
		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(path);
			bw = new BufferedWriter(fw);
			bw.write(content);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		finally {
			if(bw != null) 
				bw.close();
			if(fw != null)
				fw.close();
		}
		
		System.out.println("Done");
	}
}
