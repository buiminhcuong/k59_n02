package stream_demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadAllBytesDemo {

	public static void main(String[] args) {
		String content = "";

		try {
			byte[] bytes = Files.readAllBytes(Paths.get("C:\\java\\file1.txt"));
			content = new String(bytes);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		System.out.println(content);
	}

}
