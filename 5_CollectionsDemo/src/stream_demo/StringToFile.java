package stream_demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class StringToFile {

	public static void main(String[] args) {
		String content = "Hello World \r\nJava!\r\n";
		String path = "c:\\java\\file2.txt";
		try {
			Files.write(Paths.get(path), content.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			throw new RuntimeException(e);

		}
	}

}
