package list_demo;

import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;

public class Person {
	private String id;
	private String name;

	public Person(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Person) {
			Person another = (Person) obj;
			if (this.id.equals(another.id)) {
				return true;
			}
		}
		return false;
	}
	
	public int hashCode() {
		return id.hashCode();
	}

	
	public static void main(String[] args) {
		Set<Person> allPeople = new HashSet<Person>();
		FileReader fr;
		Person p1 = new Person("1", "An");
		System.out.println(p1.hashCode());
		allPeople.add(p1);

		Person p2 = new Person("1", "An");
		System.out.println(p2.hashCode());
		allPeople.add(p2);
		
		System.out.println(allPeople.size());
	}
}
