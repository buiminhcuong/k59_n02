package thread_demo;

public class CalculatorThread extends Thread {
	private int from;
	private int to;
	private long sum;

	public CalculatorThread(int from, int to) {
		this.from = from;
		this.to = to;
	}

	public void run() {
		System.out.println("sum from: " + from + "; to: " + to);
		sum = 0;
		for (int i = from; i < to; i++) {
			sum += i;
		}
	}

	public long getSum() {
		return sum;
	}
}
