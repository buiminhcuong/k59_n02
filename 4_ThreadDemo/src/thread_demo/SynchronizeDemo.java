package thread_demo;

public class SynchronizeDemo extends Thread{
	private int n;
	private Printer t;
	
	public SynchronizeDemo(Printer t, int n) {
		this.t = t;
		this.n = n;
	}
	
	public void run() {
		t.print(n);
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");
		Printer t = new Printer();
		SynchronizeDemo s1 = new SynchronizeDemo(t, 2);
		SynchronizeDemo s2 = new SynchronizeDemo(t, 3);
		
		s1.start();
		s2.start();
		
		s1.join();
		s2.join();
		
		System.out.println("End");

	}

}
