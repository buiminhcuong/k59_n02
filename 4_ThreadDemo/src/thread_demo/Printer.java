package thread_demo;

public class Printer {
	public synchronized void print(int n) {
		for (int i = 0; i < 10; i++) {
			System.out.println(i * n);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
