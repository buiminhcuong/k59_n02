package thread_demo;

public class CalculatorDemo {

	public static void main(String[] args) throws InterruptedException {
		int upperBound = 1000000000;
		int totalThreads = 10;
		
		int from = 0;
		int to = 0;
		int step = upperBound / totalThreads;
		
		CalculatorThread[] threads = new CalculatorThread[totalThreads];
		long start = System.currentTimeMillis();
		for(int i = 0; i < threads.length; i++) {
			from = step * i;
			to = step * (i + 1);
			threads[i] = new CalculatorThread(from, to);
			threads[i].start();
		}
		
		long sum = 0;
		for(int i = 0; i < threads.length; i++) {
			threads[i].join();
			sum += threads[i].getSum();
		}		
		
		long end = System.currentTimeMillis();
		
		System.out.println("Sum: " + sum);
		System.out.println("Total time: " + (end - start));
	}

}
