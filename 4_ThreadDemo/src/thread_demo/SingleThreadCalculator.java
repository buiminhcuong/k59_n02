package thread_demo;

public class SingleThreadCalculator {
	public long sum() {
		long result = 0;
		for (int i = 0; i < 1000000000; i++) {
			result += i;
		}
		return result;
	}

	public static void main(String[] args) {
		SingleThreadCalculator c = new SingleThreadCalculator();
		long start = System.currentTimeMillis();
		long result = c.sum();
		long end = System.currentTimeMillis();
		System.out.println("sum: " + result + "; total time: " + (end - start) + "ms");
	}

}
