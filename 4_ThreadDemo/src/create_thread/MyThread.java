package create_thread;

public class MyThread extends Thread{
	
	public void run() {
		System.out.println("Start Mythread");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.println("End Mythread");
	}

	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");
		MyThread t1 = new MyThread();
		t1.start();
		t1.join();
		t1.start();
		System.out.println("End");
	}
}
