package create_thread;

public class MyThread2 implements Runnable{
	@Override
	public void run() {
		System.out.println("Start Mythread2");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		System.out.println("End Mythread2");
		
	}
	
	public static void main(String[] args) throws InterruptedException {
		System.out.println("Start");
		MyThread2 t2 = new MyThread2();
		Thread t = new Thread(t2);
		t.start();
		t.join();
		System.out.println("End");
	}



}
