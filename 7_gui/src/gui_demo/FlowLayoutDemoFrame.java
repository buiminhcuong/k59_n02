package gui_demo;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FlowLayoutDemoFrame extends JFrame{
	public FlowLayoutDemoFrame() {
		JPanel centerPanel = new JPanel();
		centerPanel.add(new JButton("Confirm"));
		centerPanel.add(new JTextField(30));
		centerPanel.add(new JLabel("Flow layout demo"));
		centerPanel.setBackground(Color.BLUE);
		add(centerPanel);
		
		JPanel bottomPanel = new JPanel();
		bottomPanel.add(new JButton("bottom"));
		bottomPanel.setBackground(Color.RED);
		add(BorderLayout.SOUTH, bottomPanel);
		
		setTitle("Flow layout demo");
		setSize(600, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String[] args) {
		FlowLayoutDemoFrame f1 = new FlowLayoutDemoFrame();
		f1.setVisible(true);
	}
}
