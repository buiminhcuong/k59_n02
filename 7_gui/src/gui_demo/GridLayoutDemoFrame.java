package gui_demo;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GridLayoutDemoFrame extends JFrame {
	private JPanel topPanel = new JPanel();
	private JPanel centerPanel = new JPanel();
	
	public GridLayoutDemoFrame() {
		topPanel.add(new JTextField(20));
		add(BorderLayout.NORTH, topPanel);
		
		centerPanel.setLayout(new GridLayout(3, 4));
		for (int i = 0; i < 10; i++) {
			centerPanel.add(new JButton(String.valueOf(i)));
		}
		add(BorderLayout.CENTER, centerPanel);
		
		setTitle("Grid layout demo");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		GridLayoutDemoFrame f1 = new GridLayoutDemoFrame();
		f1.setVisible(true);
	}
}
