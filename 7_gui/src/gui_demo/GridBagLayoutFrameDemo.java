package gui_demo;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class GridBagLayoutFrameDemo extends JFrame{
	GridBagLayoutFrameDemo() {
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 0;
		add(new JButton("1"), c);
		c.weightx = 2;
		c.gridx = 1;
		add(new JButton("2"), c);
		c.weightx = 1;
		c.gridx = 2;
		add(new JButton("3"), c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;

		add(new JButton("4"), c);
		
		setTitle("Grid bag layout demo");
		setSize(600, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		GridBagLayoutFrameDemo f1 = new GridBagLayoutFrameDemo();
		f1.setVisible(true);
	}

}
