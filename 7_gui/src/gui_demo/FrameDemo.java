package gui_demo;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class FrameDemo {

	public static void main(String[] args) {
		JFrame f1 = new JFrame();
		
		f1.add(BorderLayout.NORTH, new JButton("NORTH"));
		f1.add(BorderLayout.SOUTH, new JButton("SOUTH"));
		f1.add(BorderLayout.WEST, new JButton("WEST"));
		f1.add(BorderLayout.EAST, new JButton("EAST"));
		f1.add(BorderLayout.CENTER, new JButton("CENTER"));
		
		f1.setTitle("Hello");
		f1.setSize(600, 400);
		f1.setLocationRelativeTo(null);
		f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f1.setVisible(true);
	}
}
