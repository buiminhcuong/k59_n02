import java.util.Scanner;
public class AgeDemo {
	public static void main(String[] args) {
		int i = 0;
		Scanner sc = new Scanner(System.in);
		do {
			System.out.print("Enter number between [0,100]: ");
			i = sc.nextInt();
		}
		while(i < 0 || i > 100);
		System.out.println("Your number: " + i);
	}
}

