
public class StringDemo3 {

	public static void main(String[] args) {
		String s = " Nguyen    Van  An    ";
		String[] sArr = s.split(" ");
		String firstName = sArr[sArr.length - 1];
		String lastName = "";
		for(int i = 0; i < sArr.length - 2; i++) {
			if(!sArr[i].equals("")) {
				lastName += sArr[i] + " ";
			}
		}
		
		System.out.println("Fist name: " + firstName);
		System.out.println("Last name: " + lastName.trim());
	}
}
