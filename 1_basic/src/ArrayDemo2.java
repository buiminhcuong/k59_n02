
public class ArrayDemo2 {

	public static void main(String[] args) {
		int[] arr = new int[] {10, 11, 12, 13, 14, 15};
		System.out.println("cach 1");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		
		System.out.println("cach 2");
		for(int i : arr) {
			System.out.println(i);
		}
	}

}
