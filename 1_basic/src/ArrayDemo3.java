import java.util.Scanner;

public class ArrayDemo3 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("nhap n:");
		int n = scanner.nextInt();
		int[] a = new int[n];
		System.out.println("nhap cac phan tu:");
		for (int i = 0; i < n; i++) {
			a[i] = scanner.nextInt();
		}
		
		int s = 0;
		for (int i = 0; i < n; i++) {
			System.out.print(a[i] + "\t");
			s += a[i];
		}
		System.out.println();
		System.out.println("tong cac phan tu:" + s);
	}
}
