package collection_demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WordDemo {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a sentense: ");
		String s = sc.nextLine();
		
		Map<String, Integer> wordCount = countWords(s);
		for(String word : wordCount.keySet()){
			System.out.println(word +": " + wordCount.get(word));
		}
	}
	
	private static Map<String, Integer> countWords(String s) {
		s = s.replace(".", "").replace(",", "");
		String[] arr = s.split(" ");
		Map<String, Integer> wordCount = new HashMap<String, Integer>();
		
		for(String word : arr) {
			int count = 1;
			if(wordCount.containsKey(word)) {
				count = wordCount.get(word) + 1;
			}
			wordCount.put(word, count);
		}
		
		return wordCount;
	}
}
