package collection_demo;

import java.util.ArrayList;
import java.util.List;

public class ListDemo {

	public static void main(String[] args) {
		List<Long> list1 = new ArrayList<Long>();
		list1.add(3L);
		list1.add(45L);
		list1.add(5L);
		list1.add(8L);
		list1.add(20L);
		
		System.out.println("Index of 5: " + list1.indexOf(5L));
	}
}
