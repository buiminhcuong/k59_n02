package collection_demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetDemo1 {

	public static void main(String[] args) {
		List<String> list1  = new ArrayList<String>();
		list1.add("Hello");
		list1.add("Hello");
		System.out.println(list1.size());
		
		Set<String> set1 = new HashSet<String>();
		set1.add("Hello");
		set1.add("Hello");
		System.out.println(set1.size());

	}

}
