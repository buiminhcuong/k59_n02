package collection_demo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyDemo {

	public static void main(String[] args) throws IOException {
		try(FileInputStream fi = new FileInputStream("C:\\java\\file3.txt");
				FileOutputStream fo = new FileOutputStream("C:\\java\\file2.txt")) {
			int c;
			while((c = fi.read()) != -1) {
				fo.write(c);
			}
		}
		
		System.out.println("Done");
	}
}
