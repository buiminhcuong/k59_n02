package collection_demo;

import java.util.HashSet;
import java.util.Set;

public class SetDemo2 {

	public static void main(String[] args) {
		Set<Person> set1 = new HashSet<Person>();
		
		Person p1 = new Person(1, "An");
		set1.add(p1);
		
		Person p2 = new Person(1, "An");
		set1.add(p2);
		
		System.out.println(set1.size());
	}

}
