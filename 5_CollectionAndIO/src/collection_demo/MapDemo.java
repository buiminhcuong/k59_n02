package collection_demo;

import java.util.HashMap;
import java.util.Map;

public class MapDemo {

	public static void main(String[] args) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		map.put("mystring1", 1);
		map.put("mystring2", 2);
		
		for(String key : map.keySet()) {
			System.out.println("key: " + key + "; value: " + map.get(key));
		}
	}

}
