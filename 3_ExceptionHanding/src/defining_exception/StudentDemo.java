package defining_exception;

public class StudentDemo {

	public static void main(String[] args){
		Student s1 = new Student();
		double mathMark = -6.7; //input fom keyboard, file...
		
		try {
			s1.setMath(mathMark);
		} catch (InvalidMarkValueException e) {
			System.err.println("Mark value error: " + e.getMessage());
		}
		
		System.out.println("Student math mark: " + s1.getMath());
	}
}
