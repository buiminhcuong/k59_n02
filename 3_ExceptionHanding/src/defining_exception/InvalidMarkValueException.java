package defining_exception;

public class InvalidMarkValueException extends Exception{
	public InvalidMarkValueException(String message) {
		super(message);
	}
}
