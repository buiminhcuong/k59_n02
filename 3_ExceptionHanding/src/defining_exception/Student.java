package defining_exception;

import java.io.IOException;

public class Student {
	private double math;

	public double getMath() {
		return math;
	}

	public void setMath(double math) throws InvalidMarkValueException{
		if (!isValidMark(math)) {
			throw new InvalidMarkValueException("Math must be between [0, 10]");
		}
		
		this.math = math;
	}

	public static boolean isValidMark(double mark) {
		return mark >= 0 && mark <= 10;
	}
}
