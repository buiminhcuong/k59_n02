package memory_demo;

public class Person {
	public String name;

	public Person(String name) {
		this.name = name;
	}
	
	public void finalize() {
		System.out.println("Destructor of Person: " + name);
	}
}
