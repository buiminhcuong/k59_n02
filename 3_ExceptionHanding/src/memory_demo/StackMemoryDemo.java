package memory_demo;

public class StackMemoryDemo {

	public static void main(String[] args) {
		int i = 10;
		int j = 11;
		method1();
		System.out.println(i);
	}

	private static void method1() {
		long k = 1000;
		method2(k);
		long l = 2000;
	}

	private static void method2(long k) {
		k = 3000;
		double d = 67.7;
	}

}
