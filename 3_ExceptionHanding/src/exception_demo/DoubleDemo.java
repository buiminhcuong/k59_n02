package exception_demo;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DoubleDemo {

	public static void main(String[] args) {
		double d = readDouble("Enter a double value within [0, 10000]: ", 0 , 10000);
		System.out.println("You entered: " + d);
	}
	
	private static double readDouble(String message, double lowerBound, double upperBound) {
		Scanner sc = new Scanner(System.in);
		double result = 0;
		boolean isCorrect = true;
		do {
			System.out.print(message);
			
			try {
				result = sc.nextDouble();
			}
			catch(InputMismatchException e) {
				System.out.println("Enter a valid number");
				isCorrect = false;
				sc.nextLine();
				continue;
			}
			
			isCorrect = isValueWithinRange(result, lowerBound, upperBound);
		} while (!isCorrect);
		
		return result;
	}

	private static boolean isValueWithinRange(double result, double lowerBound, double upperBound) {
		boolean isCorrect;
		if (result < lowerBound || result > upperBound) {
			System.out.println("Data must be between [" + lowerBound +", " + upperBound + "]");
			isCorrect = false;
		}
		else {
			isCorrect = true;
		}
		return isCorrect;
	}

}
