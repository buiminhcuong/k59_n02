package exception_demo;

public class MultipleCatchDemo {
	public static void main(String[] args) {
		tryMultipleCatchesDemo();
	}

	private static void tryMultipleCatchesDemo() {
		try {
			int[] arr = new int[2];
			//System.out.println(arr[3]);
			System.out.println(10/0);
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("array index out of bound error: " + e.getMessage());
		}
		catch(ArithmeticException e) {
			System.out.println("arithmetic exception:" + e.getMessage());
		}
		catch(Exception e) {
			System.out.println("Error occurred: " + e.getMessage());
		}
	}
}

//chuong trinh co dung dot ngot hay khong