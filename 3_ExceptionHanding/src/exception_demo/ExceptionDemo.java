package exception_demo;

public class ExceptionDemo {
	public static void main(String[] args) {
		arrayOutput();
		System.out.println("Finish");
	}

	private static void arrayOutput() {
		try {
			int[] arr = new int[3];
			arr[0] = 5;
			arr[1] = 6;
			arr[2] = 7;
			
			System.out.println(arr[5]);
			System.out.println("End try");
		}
		finally{
			System.out.println("Finally");
		}
	}
}
//1. End try;Finally;Finish
//2. Finally;Finish
//3. Finally
//4. Finish