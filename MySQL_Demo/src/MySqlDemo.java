import java.sql.*;

public class MySqlDemo {

	static final String DRIVER_CLASS = "com.mysql.cj.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost/demo?serverTimezone=UTC";
	static final String USER = "root";
	static final String PASS = "";

	public static void main(String[] args) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		Statement stmt = null;
		try {
			System.out.println("STEP 1: Register JDBC driver");
			Class.forName(DRIVER_CLASS);

			System.out.println("STEP 2: Open a connection");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			System.out.println("STEP 3: Execute a query");
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT id, name, address FROM Person");
			
			System.out.println("STEP 4: Extract data from result set");
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");

				System.out.print("ID: " + id);
				System.out.print(", name: " + name);
				System.out.println(", address: " + address);
			}
			rs.close();

		}catch (SQLException e) {
			throw e;
		} finally {
			System.out.println("STEP 5: Close connection");
			if (stmt != null)
				stmt.close();
			if (conn != null)
				conn.close();
			
		} 
		System.out.println("Done!");
	}
}