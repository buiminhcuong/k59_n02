package hinh_demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HinhDemo {

	public static void main(String[] args) {
		List<Hinh> list = new ArrayList<Hinh>();

		do {
			int choice = readInt("Chọn hình muốn nhập (0. Ket thuc, 1. Tam Giac, 2. Chu Nhat, 3. Hinh Tron): ", 0, 3);
			if(choice == 0) break;
			
			Hinh input = null;
			switch (choice) {
			case 1:
				input = new TamGiac(
						readDiem("Nhập điểm A"), 
						readDiem("Nhập điểm B"), 
						readDiem("Nhập điểm C"));
				break;
			case 2:
				input = new ChuNhat(
						readDouble("Nhập cạnh a: ", 0, Double.MAX_VALUE), 
						readDouble("Nhập cạnh b: ", 0, Double.MAX_VALUE));
				break;
			case 3:
				input = new HinhTron(readDiem("Nhập điểm o"), 
						readDouble("Nhập bán kính r", 0, Double.MAX_VALUE));
				break;
			default:
				break;
			}
			list.add(input);
			
		} while(true);

		System.out.println("Danh sach cac hinh");
		for (Hinh hinh : list) {
			print(hinh);
		}
	}
	
	private static Diem readDiem(String message) {
		Scanner sc = new Scanner(System.in);
		System.out.println(message);
		double x = readDouble("Nhập điểm x: ", Double.MIN_VALUE, Double.MAX_VALUE);
		double y = readDouble("Nhập điểm y: ", Double.MIN_VALUE, Double.MAX_VALUE);
		return new Diem(x, y);
	}
	
	private static int readInt(String message, int lowerBound, int upperBound) {
		Scanner sc = new Scanner(System.in);
		int n;
		do {
			System.out.print(message);
			n = sc.nextInt();
			if (n < lowerBound || n > upperBound) {
				System.out.println("Dữ liệu phải trong khoảng [" + lowerBound +", " + upperBound + "]");
			}
		} while (n < lowerBound || n > upperBound);
		
		return n;
	}
	
	private static double readDouble(String message, double lowerBound, double upperBound) {
		Scanner sc = new Scanner(System.in);
		double result;
		do {
			System.out.print(message);
			result = sc.nextDouble();
			if (result < lowerBound || result > upperBound) {
				System.out.println("Dữ liệu phải trong khoảng [" + lowerBound +", " + upperBound + "]");
			}
		} while (result < lowerBound || result > upperBound);
		
		return result;
	}

	private static void print(Hinh h) {
		System.out.println(h.toString());
		if (!h.isValidIput()) {
			System.out.println("Du lieu dau vao khong dung");
		} else {
			System.out.println("Chu vi: " + h.chuVi());
			System.out.println("Dien tich: " + h.dienTich());
		}
	}
}
