package hinh_demo;

public abstract class Hinh {
	public abstract boolean isValidIput();
	
	public abstract double chuVi();
	
	public abstract double dienTich();
}
