package hinh_demo;

public class ChuNhat extends Hinh{
	private double a;
	private double b;
	
	public ChuNhat(double a, double b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	public boolean isValidIput() {
		return a > 0 && b > 0;
	} 
	
	public double chuVi() {
		return (a + b) * 2;
	}
	
	public double dienTich() {
		return a * b;
	}
	
	public String toString() {
		return "Chu Nhat: a=" + a + ";b=" + b;
	}
}
