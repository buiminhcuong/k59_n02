package hinh_demo;

public class DiemDemo {
	public static void main(String[] args) {
		Diem d1 = new Diem(1, 3);
		Diem d2 = new Diem(5, 6.5);
		DuongThang dt = new DuongThang(d1, d2);
		System.out.println(dt.doDai());
	}
}
