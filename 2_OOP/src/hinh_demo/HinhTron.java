package hinh_demo;

public class HinhTron extends Hinh{
	private Diem o;
	private double r;
	
	public HinhTron(Diem o, double r) {
		super();
		this.o = o;
		this.r = r;
	}
	
	public boolean isValidIput() {
		return r > 0;
	} 
	
	public double chuVi() {
		return 2 * Math.PI * r;
	}
	
	public double dienTich() {
		return Math.PI * r * r;
	}
	
	public String toString() {
		return "Hinh Tron: o=" + o + ";r=" + r;
	}
}
