package hinh_demo;

public class TamGiac extends Hinh{
	private Diem a;
	private Diem b;
	private Diem c;
	private double ab;
	private double bc;
	private double ca;

	public TamGiac(Diem a, Diem b, Diem c) {
		this.a = a;
		this.b = b;
		this.c = c;
		ab = new DuongThang(a, b).doDai();
		bc = new DuongThang(b, c).doDai();
		ca = new DuongThang(c, a).doDai();
	}

	public boolean isValidIput() {
		return !(ab == bc + ca || bc == ca + ab || ca == ab + bc);
	}

	public double chuVi() {
		return ab + bc + ca;
	}

	public double dienTich() {
		double p = chuVi() / 2;
		return Math.sqrt(p * (p - ab) * (p - bc) * p - (ca));
	}

	public String toString() {
		return "Tam Giac: a=" + a + ";b=" + b + "; c=" + c;
	}
}
