package constructor_demo;

public class Person {
	public String name;
	public String address;
	public int birthYear;
	
	public Person(String name, String address, int birthYear) {
		this(name, address);
		this.birthYear = birthYear;
	}
	
	public Person(String name, String address) {
		this.name = name;
		this.address = address;
	}
	
	public Person() {
	}
	
	public static void main(String[] args) {
		Person p1 = new Person("An", "Ha Noi", 1990);
		printPerson(p1);
		
		Person p2 = new Person("Binh", "Nam Dinh");
		printPerson(p2);
		
		Person p3 = new Person();
	}

	private static void printPerson(Person p1) {
		System.out.println(p1.name + ";" + p1.address + ";" + p1.birthYear);
	}
}
