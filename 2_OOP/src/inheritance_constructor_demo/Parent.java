package inheritance_constructor_demo;

public class Parent {
	public int p1;
	
	public Parent(int p1) {
		System.out.println("Parent constructor");
		this.p1 = p1;
	}
}
