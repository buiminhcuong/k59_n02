package inheritance_constructor_demo;

public class ConstructorDemo {

	public static void main(String[] args) {
		Children c1 = new Children(1, 3);
		System.out.println("p1: " + c1.p1 + "; c2: " + c1.p2);
	}

}
