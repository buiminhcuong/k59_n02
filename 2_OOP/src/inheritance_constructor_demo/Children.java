package inheritance_constructor_demo;

public class Children extends Parent{
	public int p2;
	
	public Children(int p1, int p2) {
		super(p1);
		this.p2 = p2;
	}
}
