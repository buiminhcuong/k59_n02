package static_demo;

public class StaticDemo {

	public static void main(String[] args) {
		Class1 c1 = new Class1();
		System.out.println(c1.count);
		
		Class1 c2 = new Class1();
		System.out.println(Class1.count);
		
		Class1 c3 = new Class1();
		c3.count += 5;
		System.out.println(c3.count);
		
		Class1 c4 = new Class1();
		System.out.println(Class1.count);
		
		c4.print();
		Class1.print();
	}

}
