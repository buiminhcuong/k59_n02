
public class debug_demo {

	public static void main(String[] args) {
		int s = 0;
		
		for (int i = 0; i < 100; i++) {
			s += multiple(i);
		}
		
		System.out.println(s);
	}

	private static int multiple(int i) {
		int result = i * 5;
		return result;
	}

}
