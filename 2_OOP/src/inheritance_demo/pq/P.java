package inheritance_demo.pq;

public class P {
	public void printP() {
		System.out.print("P1");
	}
	
	public void printQ() {
		System.out.print("Q1");
	}
	
	public void printPQ() {
		printP();
		printQ();
	}
}
