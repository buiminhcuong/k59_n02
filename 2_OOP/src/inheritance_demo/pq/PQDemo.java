package inheritance_demo.pq;

public class PQDemo {

	public static void main(String[] args) {
		P p = new P();
		p.printPQ();
		
		p = new Q();
		p.printPQ();
		
		//1. P1Q1 P1Q1
		
		//2. P1Q1 P1Q2
	}

}
