package package1;

public class Class1 {
	public void method1() {
		System.out.println("method1");
	}
	
	protected void method2() {
		System.out.println("method2");
	}
	
	void method3() {
		System.out.println("method3");
		method4();
	}
	
	private void method4() {
		System.out.println("method4");
	}
}
