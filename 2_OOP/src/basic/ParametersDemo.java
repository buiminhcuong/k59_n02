package basic;

public class ParametersDemo {

	public static void main(String[] args) {
		int i = 1;
		changeit(i);
		System.out.println(i);
		
		Person p1 = new Person();
		p1.id = 2;
		changeit2(p1);
		System.out.println(p1.id);
	}
	
	public static void changeit(int i) {
		i = 10;
	}
	
	public static void changeit2(Person p) {
		p.id = 5;
	}
}
