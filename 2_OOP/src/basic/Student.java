package basic;

import java.security.InvalidParameterException;

public class Student {
	private String name;
	private float math;
	private float physics;
	private float chemistry;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMath(float value) {
		checkMark(value);
		math = value;
	}
	
	public void setChemistry(float value) {
		checkMark(value);
		chemistry = value;
	}
	
	public void setPhysics(float value) {
		checkMark(value);
		physics = value;
	}
	
	public float getMath() {
		return math;
	}

	public float getPhysics() {
		return physics;
	}

	public float getChemistry() {
		return chemistry;
	}

	private void checkMark(float value) {
		if(value < 0 || value > 10) {
			throw new InvalidParameterException("Mark must in range [0, 10]");
		}
	}
	
	public float avg() {
		return (math + physics + chemistry) / 3;
	}
	
	public void print() {
		System.out.println(String.format("%-20s %5.2f %5.2f %5.2f", name, math, physics, chemistry));
	}
}
