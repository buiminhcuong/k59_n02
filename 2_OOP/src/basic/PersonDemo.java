package basic;

public class PersonDemo {
	public static void main(String[] args) {
		Person p1 = new Person();
		p1.id = 1;
		p1.name = "An";
		p1.print();
		
		Person p2 = new Person();
		p2.id = 2;
		p2.name = "Binh";
		p2.print();
	}
}
