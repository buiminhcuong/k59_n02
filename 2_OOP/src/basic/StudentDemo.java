package basic;

import java.util.Scanner;

public class StudentDemo {

	public static void main(String[] args) {
		int n = inputInt("How many students?");
		Student[] s = new Student[n];

		for (int i = 0; i < s.length; i++) {
			s[i] = new Student();
			s[i].setName(inputString("Name: "));
			s[i].setMath(inputFloat("Math: "));
			s[i].setPhysics(inputFloat("Physic: "));
			s[i].setChemistry(inputFloat("Chemistry: "));
		}

		System.out.println("-------------------------------------");
		System.out.println("Students list");
		for (int i = 0; i < s.length; i++) {
			s[i].print();
		}

		System.out.println("-------------------------------------");
		System.out.println("Students wih avg > 5");
		for (int i = 0; i < s.length; i++) {
			if (s[i].avg() > 5)
				s[i].print();
		}
	}

	public static int inputInt(String message) {
		System.out.print(message);
		Scanner sc = new Scanner(System.in);
		return sc.nextInt();
	}

	public static String inputString(String message) {
		System.out.print(message);
		Scanner sc = new Scanner(System.in);
		return sc.nextLine();
	}

	public static float inputFloat(String message) {
		System.out.print(message);
		Scanner sc = new Scanner(System.in);
		return sc.nextFloat();
	}

}
