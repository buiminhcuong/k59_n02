package basic;

public class OverloadDemo {
	public static void method1() {
		System.out.println("1");
	}
	
	public static void method1(String s) {
		System.out.println("2");
	}

	public static void method1(int i) {
		System.out.println("3");
	}

	public static void main(String[] args) {
		System.out.println(5);
		System.out.println("hello");
	}
}
