package db_demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadDataDemo {
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		Map<String, String> config = readConfigFromFile();
		
		Class.forName(config.get("DRIVER_CLASS"));
		try(Connection conn = 
				DriverManager
					.getConnection(
						config.get("DB_URL"), 
						config.get("DB_USERNAME"), 
						config.get("DB_PASSWORD"));
				Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery("SELECT id, name, address FROM person");
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String address = rs.getString("address");

				System.out.print("ID: " + id);
				System.out.print(", name: " + name);
				System.out.println(", address: " + address);
			}
			rs.close();
		}
		
		System.out.println("Done");
	}

	private static Map<String, String> readConfigFromFile() throws IOException {
		List<String> text = Files.readAllLines(Paths.get("config.txt"));
		Map<String, String> config = new HashMap<String, String>();
		for(String line : text) {
			String[] arr = line.split("=", 2);
			if(arr.length > 1) {
				config.put(arr[0], arr[1]);
			}
			else {
				config.put(arr[0], "");
			}
		}
		
		return config;
	}

}
